"""
Unit Tests for Jira Issues

Directly fetching data from Jira.
"""
import os
import logging
from test import (
        file_path,
        setup_environment_variables,
        teardown_environment_variables,
        )
import pytest
from jira_log.jira_log import save_data
from jira_log.date_time import current_month, JIRA_REFERENCE_DATE
from jira_log.allocation import create_dataset
from jira_log.csv_creation import (
    QA_STATUS,
    QA_CSV,
    WIP_STATUS,
    WIP_CSV,
    UIUX_STATUS,
    UIUX_CSV,
    create_file,
)
from jira_log import JQL_MONTHLY, OUTPUT_FOLDER


FORMAT = '%(asctime)-15s %(fname)s:%(fid)s %(messages)s'
logging.basicConfig(filename='test_issues.log',
                    level=logging.INFO)


def setup_module(module):
    setup_environment_variables()


def teardown_module(module):
    teardown_environment_variables()


def test_issues_data():
    jira_log = 'jira_log_20200101.pkl'
    if not os.path.isfile(file_path(jira_log)):
        pkl_file = save_data(JQL_MONTHLY.format('2020-01-01'))
        assert os.path.isfile(file_path(pkl_file))
    else:
        assert True


@pytest.mark.dec2k19
@pytest.mark.skip(reason='To be tested on as needed basis')
def test_issues_dec_2k19():
    os.environ[JIRA_REFERENCE_DATE] = '2019-12-31'
    day1, _, _, reference = current_month()
    jira_log = 'dec_2k19_{today}.pkl'
    if not os.path.isfile(file_path(jira_log)):
        pkl_file = save_data(JQL_MONTHLY.format(day1), file_prefix='dec_2k19')
    else:
        pkl_file = jira_log
    assert file_path(pkl_file) == file_path(jira_log)
    dataset = create_dataset(pkl_file)
    wip_csv = WIP_CSV.format(f'_{reference}')
    qa_csv = QA_CSV.format(f'_{reference}')
    uiux_csv = UIUX_CSV.format(f'_{reference}')
    create_file(dataset, WIP_STATUS, wip_csv)
    create_file(dataset, QA_STATUS, qa_csv)
    create_file(dataset, UIUX_STATUS, uiux_csv)
    assert os.path.isfile(file_path(wip_csv))
    assert os.path.isfile(file_path(qa_csv))
    assert os.path.isfile(file_path(uiux_csv))


@pytest.mark.curmon
def test_issues_for_current_month():
    day_1, today, _, reference = current_month()
    jira_log = f'jira_log_{today}.pkl'
    if not os.path.isfile(file_path(jira_log)):
        pkl_file = save_data(JQL_MONTHLY.format(day_1))
    else:
        pkl_file = jira_log
    assert file_path(pkl_file) == file_path(jira_log)
    wip_csv = WIP_CSV.format(f'_{reference}')
    qa_csv = QA_CSV.format(f'_{reference}')
    uiux_csv = UIUX_CSV.format(f'_{reference}')
    dataset = create_dataset(pkl_file)
    assert dataset.DateCreated.isnull().sum() == 0
    create_file(dataset, WIP_STATUS, wip_csv)
    create_file(dataset, QA_STATUS, qa_csv)
    create_file(dataset, UIUX_STATUS, uiux_csv)
    assert os.path.isfile(file_path(wip_csv))
    assert os.path.isfile(file_path(qa_csv))
    assert os.path.isfile(file_path(uiux_csv))


def test_uiux_issues_for_current_month():
    day_1, today, _, reference = current_month()
    jira_log = f'jira_log_{today}.pkl'
    if not os.path.isfile(file_path(jira_log)):
        pkl_file = save_data(JQL_MONTHLY.format(day_1))
    else:
        pkl_file = jira_log
    assert file_path(pkl_file) == file_path(jira_log)
    uiux_csv = UIUX_CSV.format(f'_{reference}')
    dataset = create_dataset(pkl_file)
    assert dataset.DateCreated.isnull().sum() == 0
    create_file(dataset, UIUX_STATUS, uiux_csv)
    assert os.path.isfile(file_path(uiux_csv))
