"""
Test Module
"""
import os
from jira_log import OUTPUT_FOLDER, CONFIG_FOLDER


def file_path(file_name):
    """Returns the output path for the file_name"""
    return os.path.join(os.environ.get(OUTPUT_FOLDER, '.'), file_name)


def setup_environment_variables():
    os.environ.update({CONFIG_FOLDER: './config',
                       OUTPUT_FOLDER: './test_output',
                       })


def teardown_environment_variables():
    del os.environ[CONFIG_FOLDER]
    del os.environ[OUTPUT_FOLDER]
