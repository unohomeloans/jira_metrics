"""
Unit Tests for Jira Access

Directly fetching data from Jira.
"""
import os
import logging
import pytest
from cards.jira_board import connection, search_issues, netrc_credentials
from jira_log.jira_log import (
        REMOTE_SERVER,
        fetch_asset,
        )
from jira_log import JQL, CONFIG_FOLDER


FORMAT = '%(asctime)-15s %(fname)s:%(fid)s %(messages)s'
logging.basicConfig(filename='jira_log.log',
                    level=logging.INFO)


def setup_module(module):
    os.environ.update({CONFIG_FOLDER: './config',
                       })


def teardown_module(module):
    del os.environ[CONFIG_FOLDER]


@pytest.fixture
def credentials():
    return netrc_credentials('unohomeloans.com.au')


def test_get_first_50_cards(credentials):
    conn = connection(credentials, REMOTE_SERVER)
    issues = search_issues(conn, JQL)
    assert len(issues) == 50


def test_get_all_cards(credentials):
    conn = connection(credentials, REMOTE_SERVER)
    issues = search_issues(conn, JQL, max_results=1000)
    assert len(issues) == 99


def test_get_all_changelogs(credentials):
    conn = connection(credentials, REMOTE_SERVER)
    issues = search_issues(conn, JQL, max_results=1000)
    ids = [issue.key for issue in issues]
    assert ids[0] == 'PN-170'


def test_fetch_asset(credentials):
    conn = connection(credentials, REMOTE_SERVER)
    epic_key = 'CVP-942'
    components = fetch_asset(conn, epic_key)
    assert components == 'CV2'
