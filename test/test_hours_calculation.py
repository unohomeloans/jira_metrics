# coding: utf-8
"""
Unit Tests for Total Hours Calculation
"""
import os
import datetime as dt
import pandas as pd
from jira_log.hours_calculator import (
        total_hours,
        add_holiday,
        HOLIDAYS_FILE,
        )


DT1 = dt.datetime(2020, 2, 1, 10, 23, 45)
DT2 = dt.datetime(2020, 2, 5, 14, 1, 44)


def setup_module(module):
    os.environ[HOLIDAYS_FILE] = 'holidays.pkl'


def test_total_hours():
    assert total_hours(DT1, DT2) == 22.0


def test_total_hours_during_weekends():
    dt2 = dt.datetime(2020, 2, 2, 10, 23, 45)
    assert total_hours(DT1, dt2) == 0


def test_total_hours_between_two_days():
    dt1 = dt.datetime(2020, 2, 3, 10, 23, 45)
    dt2 = dt.datetime(2020, 2, 4, 10, 23, 45)
    assert total_hours(dt1, dt2) == 8.5


def test_total_hours_within_the_same_day():
    dt1 = dt.datetime(2020, 2, 3, 10, 23, 45)
    dt2 = dt.datetime(2020, 2, 3, 14, 25, 45)
    assert total_hours(dt1, dt2) == 4.0


def test_total_hours_end_date_greater_than_start_date():
    dt1 = dt.datetime(2020, 2, 3, 10, 23, 45)
    dt2 = dt.datetime(2020, 2, 2, 14, 25, 45)
    try:
        total_hours(dt1, dt2)
        assert False, 'Exception should be thrown'
    except AssertionError:
        assert True


def test_total_hours_2():
    dt1 = dt.datetime(2020, 2, 5, 15, 12, 34)
    dt2 = dt.datetime(2020, 2, 6, 13, 34, 36)
    assert 6 < total_hours(dt1, dt2) < 7


def test_total_hours_for_different_work_times():
    dt1 = dt.datetime(2020, 2, 5, 18, 21, 34)
    dt2 = dt.datetime(2020, 2, 6, 11, 30, 36)
    reference_name = 'Siddharth Ramesh'
    assert total_hours(dt1, dt2, reference_name) == 1.7


def test_total_hours_at_midnight():
    dt1 = dt.datetime(2020, 2, 14, 10, 0, 0)
    dt2 = dt.datetime(2020, 2, 17, 8, 0, 0)
    assert total_hours(dt1, dt2) == 7.5


def test_add_holidays():
    holiday_date = dt.datetime(2020, 4, 10, 7, 34, 12)
    no_holiday_date = dt.datetime(2020, 4, 15, 9, 24, 52)
    dates = pd.DataFrame([[holiday_date, pd.NA], [no_holiday_date, pd.NA]],
                         columns=['Date', 'Holiday'])
    add_holiday(dates)
    assert dates.Holiday.sum() == 1


def test_add_holidays_no_file():
    os.environ[HOLIDAYS_FILE] = ''
    holiday_date = dt.datetime(2020, 4, 10, 7, 34, 12)
    no_holiday_date = dt.datetime(2020, 4, 15, 9, 24, 52)
    dates = pd.DataFrame([[holiday_date, pd.NA], [no_holiday_date, pd.NA]],
                         columns=['Date', 'Holiday'])
    add_holiday(dates)
    assert dates.Holiday.sum() == 0


def test_add_holidays_no_holidays():
    no_holiday_date_1 = dt.datetime(2020, 5, 10, 7, 34, 12)
    no_holiday_date_2 = dt.datetime(2020, 4, 15, 9, 24, 52)
    dates = pd.DataFrame([[no_holiday_date_1, pd.NA],
                          [no_holiday_date_2, pd.NA]],
                         columns=['Date', 'Holiday'])
    add_holiday(dates)
    assert dates.Holiday.sum() == 0
