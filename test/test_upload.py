"""
Unit Tests for Google Drive upload

Sending files to Google drive.
"""
import os
import logging
import pytest
from jira_log import CONFIG_FOLDER, OUTPUT_FOLDER
from jira_log.upload import (
        csv_upload,
        list_drive_files,
        files_info,
        FOLDER_ID,
        )


FORMAT = '%(asctime)-15s %(fname)s:%(fid)s %(messages)s'
logging.basicConfig(filename='test_upload.log',
                    level=logging.INFO)


def setup_module(module):
    os.environ[OUTPUT_FOLDER] = './test_output'
    os.environ[CONFIG_FOLDER] = './config'
    os.environ[FOLDER_ID] = '1o3brQcQ0FL3LLnwFHr-1BnabRHU70grg'


def teardown_module(module):
    del os.environ[OUTPUT_FOLDER]
    del os.environ[CONFIG_FOLDER]
    del os.environ[FOLDER_ID]


@pytest.mark.upload
def test_csv_upload():
    assert csv_upload(), 'Upload not successful'


@pytest.mark.files
def test_list_drive_files():
    files = list_drive_files()
    assert files


@pytest.mark.files
def test_files_info():
    files = files_info()
    for file in files:
        assert file.startswith('jira_')
