"""
Unit Test for Datetime Module
"""
import re
import os
from datetime import datetime, time
from jira_log import date_time as jdt


def test_current_month():
    day1, today, last_day, reference = jdt.current_month()
    assert re.match(r'20[2-9][0-9]\-[0-1][0-9]\-01', day1)
    assert re.match(r'20[2-9][0-9][0-1][0-9][0-3][0-9]', today)
    assert re.match(r'20[2-9][0-9]\-[0-1][0-9]\-([23][0198])', last_day)
    assert re.match(r'[a-z]{3}[0-9]{2}', reference)


def test_read_reference_date():
    os.environ['JIRA_REFERENCE_DATE'] = '2020-02-01'
    ref_date = jdt.read_reference_date()
    assert ref_date == datetime(2020, 2, 1)


def test_read_reference_date_with_wrong_format():
    os.environ['JIRA_REFERENCE_DATE'] = '202002-01'
    ref_date = jdt.read_reference_date()
    assert ref_date.date() == datetime.now().date()


def test_current_work_time_past_5_30():
    dt1 = jdt.current_work_time(datetime(2020, 2, 5, 18, 12, 34))
    assert time(9, 0, 0) <= dt1.time() <= time(17, 30, 0)


def test_current_work_time_before_9():
    dt1 = jdt.current_work_time(datetime(2020, 2, 5, 8, 12, 34))
    assert time(9, 0, 0) <= dt1.time() <= time(17, 30, 0)
