"""
Unit Tests for CSV creation
"""
import os
import shutil
from test import (
        file_path,
        setup_environment_variables,
        teardown_environment_variables,
        )
import pytest
from jira_log.allocation import create_dataset
from jira_log.jira_log import save_data
from jira_log.csv_creation import (
        create_file,
        WIP_STATUS,
        create_dates_dataset,
        )
from jira_log import JQL_MONTHLY, OUTPUT_FOLDER
from jira_log.date_time import current_month
from jira_log.csv_dataset import fetch_dates
from jira_log.csv_time_calculator import calculate_time
from test import file_path


def setup_module(module):
    setup_environment_variables()


def teardown_module(module):
    teardown_environment_variables()


@pytest.fixture
def dataset():
    day1, today, _, _ = current_month()
    pkl_file = f'jira_log_{today}.pkl'
    if os.path.isfile(file_path(pkl_file)):
        return create_dataset(pkl_file)
    return create_dataset(save_data(JQL_MONTHLY.format(day1)))


def wrapup(csv_file):
    shutil.os.unlink(csv_file)


def test_uiux_csv(dataset):
    _, _, _, reference = current_month()
    csv_file = f'jira_uiux_{reference}.csv'
    create_file(dataset, ('ANALYSIS', 'VALIDATION'), csv_file)
    assert os.path.isfile(file_path(csv_file))
    wrapup(file_path(csv_file))


@pytest.mark.output
def test_wip_csv_with_no_folder(dataset):
    _, _, _, reference = current_month()
    csv_file = f'jira_wip_{reference}.csv'
    create_file(dataset, WIP_STATUS, csv_file)
    assert os.path.isfile(file_path(csv_file))
    wrapup(file_path(csv_file))


@pytest.mark.output
def test_wip_csv_with_wrong_folder(dataset):
    output_folder = os.environ.get(OUTPUT_FOLDER)
    os.environ[OUTPUT_FOLDER] = './not_a_folder'
    _, _, _, reference = current_month()
    try:
        csv_file = f'jira_wip_{reference}.csv'
        create_file(dataset, WIP_STATUS, csv_file)
        assert False
    except FileNotFoundError:
        assert True
    finally:
        os.environ[OUTPUT_FOLDER] = output_folder


@pytest.mark.output
def test_wip_csv_with_folder(dataset):
    _, _, _, reference = current_month()
    csv_file = f'jira_wip_{reference}.csv'
    create_file(dataset, WIP_STATUS, csv_file)
    assert os.path.isfile(file_path(csv_file))
    wrapup(file_path(csv_file))
    wrapup(file_path(f'{csv_file.replace("jira", "verify_jira")}.pkl'))


def test_create_dates_dataset(dataset):
    dates = create_dates_dataset(dataset, WIP_STATUS)
    assert dates.DateCreated_x.notnull().sum() > 0


def test_calculate_time(dataset):
    dates, _ = fetch_dates(WIP_STATUS, dataset)
    first_ticket = dates.at[0, 'TicketId']
    times = calculate_time(dates.loc[dates.TicketId == first_ticket])
    assert times[0][3] >= 0
