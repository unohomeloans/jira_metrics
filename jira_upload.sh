# Saving current directory to return to at the end
current_dir=`pwd`

# Home directory for the application
DIRECTORY=`dirname $0`
cd $DIRECTORY

# Environment variables
OUTPUT_FOLDER=$DIRECTORY/output
HOLIDAYS_FILE=holidays.pkl
CONFIG_FOLDER=$DIRECTORY/config
FOLDER_ID=1qcXjh97IMUOq4Fb9TIR1T_Al_7V3HxYb
TAG='guscastles/jira_metrics:'$VERSION

# Check for main folders

[ ! -d "$OUTPUT_FOLDER" ] && mkdir $OUTPUT_FOLDER

if [ ! -d "$CONFIG_FOLDER" ]; then
    echo "Configuration not present!!!"
    echo "."
    exit 1
fi

# Run docker image guscastles/jira_metrics:1.8
docker run -v $OUTPUT_FOLDER:/output \
           -v $CONFIG_FOLDER:/config \
           -e HOLIDAYS_FILE=holidays.pkl \
           -e FOLDER_ID=$FOLDER_ID \
           -e OUTPUT_FOLDER=/output \
           -e JIRA_REFERENCE_DATE=$JIRA_REFERENCE_DATE \
           -e CONFIG_FOLDER=/config \
           --user $(id -u):$(id -g) \
           $TAG

# Move back to original directory
cd $current_dir
