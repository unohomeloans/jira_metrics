# Jira Metrics

Team metrics extracted from Jira, with Jupyter Notebook reports.

## Abstract

This module will upload a pair of CSV files to a specific folder under Gus' account on Google Drive. This files contain information for worktime consolidation for finances.

## Main Files

Under directory $HOME/works/python/jira_metrics

* jira_upload.sh - Shell script used by the cron job.
* crontab_for_jira_log.cron - crontab file. Set to run every day, at 11:59pm, until 31st December.
* module jira_log, in jira_log/ folder - contains all the code. Run via the __main__py file.

## Databook For The CSV Files

*Some fields are only visible in the verification files*

* **TicketId** The Jira key for the ticket
* **Type** Jira issue type, e.g., Task, Epic, Story
* **EpicId** Jira key for the Epic linked to the ticket
* **Author** Who moved the ticket or added the assignee (executor) for the ticket
* **Finance** Finance type, i.e., Build or Run
* **DateCreated** The date the action (status or assignee change) occurred - Only in the verification file
* **FromStatus** The previous status of the ticket - Only in the verification file
* **ToStatus** The destination status of the ticket - Only in the verification file
* **Assignee** The person assigned to the ticket - Only in the verification file
* **Asset** The assert associated with the Epic in Components field

## Google Drive API

Documentation can be found under the [developers site][1]

[1]: https://developers.google.com/drive/api/v3/quickstart/python

## Running the Application

> jira_ulpload.sh

## Cron Schedule

Runs everyday, at 11.59 PM, AEST

> crontab_for_jira_log.cron

Please check the [cron man page][3] for further details.

[3]: http://man7.org/linux/man-pages/man5/crontab.5.html

## Unit and Integration Tests

Covered under the `test` folder. Additional test to check the integration with the whole application is also in the folder, as a bash script file.

> test/test_integration.sh

To run the test suite, generating coverage report

```
pytest --cov=jira_log -cov-report=html
```

## Environment Variables

### Reference Date

if the environment variable `JIRA_REFERENCE_DATE` is set, then that date is taken as the current date.

Format is

> YYYY-MM-DD

### Holidays File

The holidays table is stored as a pickle file in file `holidays.pkl`. This name is referenced in the environment variable

> HOLIDAYS_FILE

This value is set automatically in the virtual environment activation script

> . ./jira_metrics/bin/activate

### Config Folder

This folder contains the `.netrc` file for the Jira REST API and the `.netrc` file, to `token.pickle`, to access the Google Drive folder.

> CONFIG_FOLDER

### Output Folder

The `.csv` and `.pkl` files are placed in this folder before upload and for verification purposes.

> OUTPUT_FOLDER

### Google Destination Folder ID

This variable indicates the destination folder in Google Drive. It is composed of a 33-character hash.

> FOLDER_ID

### Version

The docker tag version is the only environment variable set outside the `jira_upload.sh` script. For example

```
export VERSION=1.8.4
```

> VERSION

## Docker

### Building

> docker build . -t jira_metrics:#.#.#

### Running

```
docker run -v $OUTPUT_FOLDER:/output \
           -v $CONFIG_FOLDER:/config \
           -e HOLIDAYS_FILE=holidays.pkl \
           -e FOLDER_ID=$FOLDER_ID \
           -e OUTPUT_FOLDER=/output \
           -e JIRA_REFERENCE_DATE=$JIRA_REFERENCE_DATE \
           -e CONFIG_FOLDER=/config \
           --user $(id -u):$(id -g) \
           $TAG
```

### Testing

In the current directory, run


```
`pwd`/jira_upload.sh
```

The `pwd` will guarantee the full pathname for the target folders (config and output).
