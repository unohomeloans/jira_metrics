export JIRA_REFERENCE_DATE=2020-02-28
export HOLIDAYS_FILE=holidays.pkl
export CONFIG_FOLDER=`pwd`/config
#export OUTPUT_FOLDER=`pwd`/test_output
export OUTPUT_FOLDER=`pwd`/output
#export FOLDER_ID=1qcXjh97IMUOq4Fb9TIR1T_Al_7V3HxYb
export FOLDER_ID=1o3brQcQ0FL3LLnwFHr-1BnabRHU70grg
TAG='jira_metrics:1.8'
docker build . -t $TAG
docker run -v $OUTPUT_FOLDER:/output \
	   -v $CONFIG_FOLDER:/config \
	   -e HOLIDAYS_FILE=holidays.pkl \
	   -e FOLDER_ID=$FOLDER_ID \
	   -e OUTPUT_FOLDER=/output \
	   -e JIRA_REFERENCE_DATE=$JIRA_REFERENCE_DATE \
	   -e CONFIG_FOLDER=/config $TAG
date
