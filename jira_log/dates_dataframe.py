# coding: utf-8
"""
Total Hours Calculator
"""
from datetime import datetime, timedelta
import pandas as pd
from jira_log.date_time import (
    WORK_TIMES,
    GENERAL_WORK_TIME,
    )


TOTAL_SECONDS_IN_AN_HOUR = 60 * 60
TOTAL_HOURS_IN_A_DAY = 24
DATE = 'Date'


def create_dates_dataframe(date_1, date_2, reference_name):
    date_entries = [date_1 + timedelta(days=days) for days
                    in total_days(date_1, date_2)]
    final_date = None
    if not date_entries:
        if same_day(date_1, date_2):
            dataframe = pd.DataFrame({DATE: date_without_work_hours(date_2,
                                                                    reference_name)},
                                     index=[0])
            final_date = calculate_time_on_same_dates(date_1,
                                                      date_2,
                                                      reference_name)
        else:
            dataframe = pd.DataFrame({DATE: date_1}, index=[0])
            final_date = date_2
    else:
        dataframe = pd.DataFrame(date_entries, columns=[DATE])
        final_date = date_2
    return add_final_entry(dataframe, final_date)


def total_days(date_1, date_2):
    return range(0, int((date_2 - date_1).total_seconds() /
                        TOTAL_SECONDS_IN_AN_HOUR //
                        TOTAL_HOURS_IN_A_DAY))


def add_final_entry(dataframe, date):
    new_entry = pd.DataFrame({DATE: date}, index=[dataframe.shape[0]])
    return pd.concat([dataframe, new_entry], axis=0)


def same_day(date_1, date_2):
    return date_1.date() == date_2.date()


def calculate_time_on_same_dates(date_1, date_2, reference_name):
    work_times = WORK_TIMES.get(reference_name, GENERAL_WORK_TIME)
    time = (date_2 - date_1).total_seconds() / TOTAL_SECONDS_IN_AN_HOUR
    hours = int(time)
    minutes = int((time - hours) * 60)
    return datetime(date_2.year, date_2.month, date_2.day,
                    work_times.start[0] + hours, minutes)


def date_without_work_hours(date, reference_name):
    work_times = WORK_TIMES.get(reference_name, GENERAL_WORK_TIME)
    return datetime(date.year, date.month, date.day, *work_times.end)
