# coding: utf-8
"""
Total Hours Calculator
"""
import os
from datetime import datetime
from pickle import load
import pandas as pd
import numpy as np
from jira_log.date_time import current_work_time
from jira_log.dates_dataframe import create_dates_dataframe


SATURDAY = 5
TOTAL_SECONDS_IN_AN_HOUR = 60 * 60
TOTAL_HOURS_IN_A_DAY = 24
WORK_START_TIME = (9, 0)
WORK_END_TIME = (17, 30)
TIME = 'Time'
HOLIDAY = 'Holiday'
WEEKDAY = 'Weekday'
HOLIDAYS_FILE = 'HOLIDAYS_FILE'


def total_hours(date_1, date_2, reference_name=None):
    dataframe = create_dates_dataframe(current_work_time(date_1,
                                                         reference_name),
                                       current_work_time(date_2,
                                                         reference_name),
                                       reference_name)
    functions = [update_time_in_dates,
                 add_time,
                 add_weekday,
                 add_holiday,
                 update_time_for_last_row,
                 ]
    for function in functions:
        dataframe = function(dataframe)

    return get_work_hours(dataframe)


def update_time_in_dates(dataframe):

    def set_start_hour(date):
        return datetime(date.year, date.month,
                        date.day, *WORK_START_TIME)

    dataframe.iloc[1:-1, 0] = dataframe.iloc[1:-1, 0].map(set_start_hour)
    return dataframe


def add_time(dataframe):
    dataframe[TIME] = dataframe.Date.map(
        lambda dt: datetime(dt.year, dt.month,
                            dt.day, *WORK_END_TIME) - dt)
    return dataframe


def add_weekday(dataframe):
    dataframe[WEEKDAY] = dataframe.Date.map(lambda dt: dt.weekday())
    return dataframe


def add_holiday(dataframe):

    def check_holiday(date):
        if isinstance(holidays, pd.DataFrame):
            return any(holidays[date.year].isin([date.date()]))
        return False

    if os.path.isfile(os.environ.get(HOLIDAYS_FILE, '')):
        holidays = load(open(os.environ.get(HOLIDAYS_FILE), 'rb'))
    else:
        holidays = None
    dataframe[HOLIDAY] = dataframe.Date.map(check_holiday)
    return dataframe


def update_time_for_last_row(dataframe):
    date = dataframe.iloc[-1, 0]
    dataframe.iat[-1, 1] = date - \
        datetime(date.year, date.month, date.day, *WORK_START_TIME)
    return dataframe


def get_work_hours(dataframe):
    valid_days = (dataframe.Weekday < SATURDAY) & (~dataframe.Holiday)
    total = dataframe.loc[valid_days, TIME].sum().total_seconds() / \
        TOTAL_SECONDS_IN_AN_HOUR
    return 0.0 if np.isnan(total) else round(total, 1)
