"""
Jira Log Upload

Provides functions to upload the CSV files to Google drive.
"""
import os
import pickle
from collections import defaultdict
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from jira_log import CONFIG_FOLDER, OUTPUT_FOLDER


FOLDER_ID = 'FOLDER_ID'
MIME_TYPE = 'application/vnd.google-apps.spreadsheet'


def csv_upload():

    def update(file_id, media):
        return service.files().update(fileId=file_id,
                                      media_body=media).execute()

    def create(file_name, media):
        file_metadata = {'name': file_name,
                         'parents': [os.environ.get(FOLDER_ID, '')],
                         'mimeType': MIME_TYPE,
                         }
        return service.files().create(body=file_metadata,
                                      media_body=media,
                                      fields='id').execute()

    creds = credentials()
    if not creds:
        return None

    files = files_info()
    service = build('drive', 'v3', credentials=creds, cache_discovery=False)
    ids = []
    for fl_name, fl_id in files.items():
        csv_file = os.path.join(os.environ.get(OUTPUT_FOLDER, '.'),
                                f'{fl_name}.csv')
        if os.path.isfile(csv_file):
            media = MediaFileUpload(csv_file, mimetype='text/csv')
            if fl_id:
                ids.append(update(fl_id, media).get(id, ''))
            else:
                ids.append(create(fl_name, media).get(id, ''))
    return ids


def credentials():
    token_file = os.path.join(os.environ.get(CONFIG_FOLDER, '.'),
                              'token.pickle')
    if not os.path.exists(token_file):
        return None

    with open(token_file, 'rb') as token:
        return pickle.load(token)


def files_info():
    drive_files = list_drive_files()
    jira_files = defaultdict(str)
    output_folder = os.environ.get(OUTPUT_FOLDER, '.')
    for _, _, files in os.walk(output_folder):
        for file in files:
            if file.startswith('jira_') and file.endswith('.csv'):
                key = file[:-4]
                jira_files[key] = drive_files.get(key, '')
    return jira_files


def list_drive_files():
    creds = credentials()
    if not creds:
        return None

    page_token = None
    files = defaultdict(str)
    folder_id = os.environ.get(FOLDER_ID, '')
    query = "mimeType='{}' and '{}' in parents".format(MIME_TYPE, folder_id)
    fields = 'nextPageToken, files(id, name)'
    service = build('drive', 'v3', credentials=creds, cache_discovery=False)
    params = {'q': query, 'spaces': 'drive',
              'fields': fields, 'pageToken': page_token}
    while True:
        response = service.files().list(**params).execute()
        for file in response.get('files', []):
            files[file.get('name')] = file.get('id')
        page_token = response.get('nextPageToken', None)
        if not page_token:
            break

    return files
