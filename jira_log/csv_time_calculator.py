#!/usr/bin/env python
# coding: utf-8
"""
Monthly Allocation Of Workers - CSV Dataset Creation
"""
from jira_log.date_time import current_date
from jira_log.hours_calculator import total_hours


def calculate_time(dates):

    def process_time(end_date):
        if previous.TicketId != row.TicketId:
            next_ticket(end_date, previous, times)
            return None
        same_ticket(row, previous, times)
        return row.DateCreated

    previous, end_date, times, row = None, None, [], None
    for row in dates.itertuples():
        if previous:
            end_date = process_time(end_date)
        previous = row
    if not times and row:
        next_ticket(end_date, previous, times)
    return times


def next_ticket(end_date, previous, times):
    if not end_date:
        if previous.Start:
            start_date = previous.DateCreated
            finish_date = current_date()
        else:
            start_date = current_date()
            finish_date = previous.DateCreated
        time = total_hours(start_date, finish_date)
        times.append((start_date, finish_date,
                      previous.TicketId, time))


def same_ticket(row, previous, times):
    if previous.Start and not row.Start:
        start_date = previous.DateCreated
        finish_date = row.DateCreated
        time = total_hours(previous.DateCreated, row.DateCreated)
        times.append((start_date, finish_date,
                      row.TicketId, time))
