"""
Jira Log Main Module
"""
from jira_log import JQL_MONTHLY
from jira_log.upload import csv_upload
from jira_log.allocation import create_dataset
from jira_log.jira_log import save_data
from jira_log.date_time import current_month
from jira_log.csv_creation import (
    WIP_STATUS,
    QA_STATUS,
    UIUX_STATUS,
    WIP_CSV,
    QA_CSV,
    UIUX_CSV,
    create_file,
)


WORK_STATE = [(WIP_STATUS, WIP_CSV),
              (QA_STATUS, QA_CSV),
              (UIUX_STATUS, UIUX_CSV),
              ]


def main():
    day_1, _, _, reference = current_month()
    dataset = create_dataset(save_data(JQL_MONTHLY.format(day_1)))
    for state, csv_file in WORK_STATE:
        create_file(dataset, state,
                    csv_file.format(f'_{reference}' if reference else ''))
    csv_upload()


if __name__ == '__main__':
    main()
