"""
Date and Time functions
"""
import os
from collections import namedtuple
from datetime import datetime, time
from calendar import monthrange
import pytz


TIME_ZONE = 'Australia/Sydney'
DATE_FORMAT = '%d%m%Y%H%M%S'
JIRA_REFERENCE_DATE = 'JIRA_REFERENCE_DATE'
WORK_START_TIME = (9, 0, 0)
WORK_END_TIME = (17, 30, 0)
WorkTimes = namedtuple('WorkTimes', ['start', 'end'])
WORK_TIMES = {'Siddharth Ramesh': WorkTimes((11, 0, 0), (19, 30, 0))}
GENERAL_WORK_TIME = WorkTimes(WORK_START_TIME, WORK_END_TIME)
TIME_UNITS = ['hour', 'minute', 'second']


def current_month():
    date = read_reference_date()
    today = date.strftime('%Y-%m-%d')
    day_1 = f'{today[:-2]}01'
    last_day = f'{today[:-2]}{monthrange(date.year, date.month)[1]} 23:59:59'
    reference = date.strftime('%b%y').lower()
    return day_1, today.replace('-', ''), last_day, reference


def read_reference_date():
    date_provided = os.environ.get(JIRA_REFERENCE_DATE, '')
    time_zone = pytz.timezone(TIME_ZONE)
    if date_provided:
        try:
            return datetime.strptime(date_provided, '%Y-%m-%d')
        except ValueError:
            pass
    return datetime.now(tz=time_zone)


def current_date():
    time_zone = pytz.timezone(TIME_ZONE)
    now = datetime.now(tz=time_zone)
    return datetime.strptime(now.strftime(DATE_FORMAT), DATE_FORMAT)


def current_work_time(date_time, reference_name=None):
    work_times = WORK_TIMES.get(reference_name, GENERAL_WORK_TIME)
    if date_time.time() > time(*work_times.end):
        return date_time.replace(**dict(zip(TIME_UNITS, work_times.end)))
    if date_time.time() < time(*work_times.start):
        return date_time.replace(**dict(zip(TIME_UNITS, work_times.start)))
    return date_time
