#!/usr/bin/env python
# coding: utf-8
"""
Monthly Allocation Of Workers
"""
import os
import datetime
from pickle import load
import pandas as pd
from jira_log import OUTPUT_FOLDER


STATUS = [('READY FOR TEST', 'READY FOR QA'),
          ('IN CODE REVIEW', 'PEER REVIEW'),
          ('IN PEER REVIEW', 'PEER REVIEW'),
          ('IN TEST', 'IN QA'),
          ]
COLUMNS = ['TicketId', 'Type', 'EpicId', 'Asset', 'Phase', 'Author',
           'DateCreated', 'FromStatus', 'ToStatus']


def create_dataset(pkl_file):
    dataset = create(load(open(os.path.join(os.environ.get(OUTPUT_FOLDER, '.'),
                                            pkl_file), 'rb')))
    return dataset


def create(jira_log):

    def issue_fields(values):
        return [(val if val else '')
                for key, val in values.items() if key != 'log']

    dataset = pd.DataFrame(columns=COLUMNS)
    for key, values in jira_log.items():
        issue_info = [key, *issue_fields(values)]
        dataset = extract_log(dataset, issue_info, values['log'])

    cleanup_date_created(cleanup_status(dataset))
    fields = ['TicketId', 'DateCreated']
    return dataset.sort_values(fields).reset_index(drop=True)


def extract_log(dataset, issue_info, log_items):

    def field_values(item):
        return {key: val for item in item.get('fields')
                for key, val in item.items()}

    def build_base_info(item):
        return [*issue_info, item.get('author'), item.get('created')]

    for item in log_items:
        base_info = build_base_info(item)
        field = field_values(item)
        from_status = field.get('status', {}).get('from', '').upper()
        to_status = field.get('status', {}).get('to', '').upper()
        info = [*base_info, from_status, to_status]
        dataset = pd.concat([dataset, pd.DataFrame([info],
                                                   columns=COLUMNS)],
                            ignore_index=True)
    return dataset


def cleanup_status(dataset):
    for stt in STATUS:
        change_status(dataset, 'FromStatus', *stt)
        change_status(dataset, 'ToStatus', *stt)

    return dataset


def cleanup_date_created(dataset):

    def time_format(date_time):
        return datetime.datetime.strptime(date_time.split('.')[0],
                                          '%Y-%m-%dT%H:%M:%S')

    dataset.DateCreated = dataset.DateCreated.apply(time_format)


def change_status(dataset, field, from_status, to_status):
    """Changes the field status from from_status to to_status"""
    dataset[field].loc[dataset[field] == from_status] = to_status
