"""
Jira Log

Directly fetching data from Jira changelog.
"""
import os
from collections import defaultdict
from pickle import dump
from cards.jira_board import connection, search_issues, netrc_credentials
from jira_log.date_time import current_month
from jira_log import OUTPUT_FOLDER


REMOTE_SERVER = 'https://planwise.jira.com'
DOMAIN = 'unohomeloans.com.au'


def save_data(jql, file_prefix='jira_log'):
    conn = connection(netrc_credentials(DOMAIN), REMOTE_SERVER)
    data = issues_data(jql, conn)
    _, today, _, _ = current_month()
    file_name = f'{file_prefix}_{today}.pkl'
    dump(data, open(os.path.join(os.environ.get(OUTPUT_FOLDER, '.'),
                                 file_name),
                    'wb'))
    return file_name


def issues_data(jql, conn):

    def get_phase(issue):
        phase = issue.fields.customfield_14575
        return phase[0].value.split('-')[0] if phase else ''

    def get_epic(fields):
        if fields.issuetype.name == 'Sub-task':
            issues = fetch_issues(f'issuekey = {fields.parent.key}', conn)
            return issues[0].fields.customfield_11210
        return fields.customfield_11210

    data = defaultdict(dict)
    for issue in fetch_issues(jql, conn):
        key = issue.key
        issue_data = search_issues(conn,
                                   f'issuekey = {key}',
                                   expand='changelog')[0]
        epic_key = get_epic(issue.fields)
        assets = fetch_asset(conn, epic_key)
        data[key] = {'type': issue.fields.issuetype.name,
                     'epic': epic_key,
                     'asset': assets,
                     'phase': get_phase(issue),
                     'log': fetch_history(issue_data.changelog.histories),
                     }
    return data


def fetch_issues(jql, conn):
    return search_issues(conn, jql, max_results=1000)


def fetch_asset(conn, epic_key):
    if not epic_key:
        return ''

    epic = search_issues(conn, f'issuekey = {epic_key}')[0]
    components = [comp.name for comp in epic.fields.components]
    return ', '.join(components)


def fetch_history(changelog):

    def get_entries(item):
        fields = defaultdict(dict)
        if item.field == 'status':
            fields['status'] = {'from': item.fromString,
                                'to': item.toString}
            data['fields'].append(fields)

    entries = []
    for entry in changelog:
        data = {'author': entry.author.displayName,
                'created': entry.created,
                'fields': [],
                }
        _ = [get_entries(item) for item in entry.items]
        if data['fields']:
            entries.append(data)
    return entries
