#!/usr/bin/env python
# coding: utf-8
"""
Monthly Allocation Of Workers - CSV Dataset Creation
"""
import datetime as dt
import pandas as pd
from jira_log.csv_time_calculator import calculate_time
from jira_log.date_time import current_month


def create_dates_dataset(dataset, status):

    def merge_data(time_df, data):
        fields = ['TimeInProgress', 'DateCreated_x', 'DateCreated_y']
        data = pd.concat([data, pd.DataFrame(columns=fields)], axis=1)
        for row in time_df.itertuples():
            tkt_fltr = data.TicketId == row.TicketId
            fltr = tkt_fltr & \
                (data.DateCreated == row.DateCreated_y) & \
                (data.FromStatus == (status if isinstance(status, str)
                                     else status[1]))
            if data.loc[fltr, fields].empty:
                fltr = tkt_fltr & \
                    (data.DateCreated == row.DateCreated_x) & \
                    (data.ToStatus == (status if isinstance(status, str)
                                       else status[0]))
            data.loc[fltr, fields] = [row.TimeInProgress, row.DateCreated_x,
                                      row.DateCreated_y]
        return data

    def remove_extra_fields(data):
        fields = ['TicketId', 'DateCreated_x', 'DateCreated_y',
                  'Type', 'EpicId', 'Asset', 'Phase', 'Author',
                  'TimeInProgress']
        return data.loc[data.TimeInProgress.notnull(), fields].copy()

    dates, data = fetch_dates(status, dataset)
    times = calculate_time(dates)
    time_df = pd.DataFrame(times, columns=['DateCreated_x', 'DateCreated_y',
                                           'TicketId', 'TimeInProgress'])
    data = merge_data(time_df[time_df.TimeInProgress > 0], data)
    return remove_extra_fields(data)


def fetch_dates(status, dataframe):

    def ticket_dates(start, end):
        dates = pd.concat([start, end], axis=0)
        dates.reset_index(inplace=True, drop=True)
        return dates.sort_values(['TicketId', 'DateCreated', 'Author'])

    def update_to_current_month(dates):
        day1, _, last_day, _ = current_month()
        dates.loc[dates.DateCreated < day1, 'DateCreated'] = \
            dt.datetime.strptime(day1, '%Y-%m-%d')
        dates.loc[dates.DateCreated > last_day, 'DateCreated'] = \
            dt.datetime.strptime(last_day, '%Y-%m-%d %H:%M:%S')
        return dates

    start, end = get_status_boundaries(dataframe, status)
    start['Start'] = True
    end['Start'] = False
    dates = ticket_dates(start, end)
    return update_to_current_month(dates), dataframe


def get_status_boundaries(dataset, status):
    if isinstance(status, tuple):
        start, end = status
    elif isinstance(status, str):
        start, end = status, status
    else:
        return None, None

    fields = ['TicketId', 'DateCreated', 'Author']
    to_status = dataset.loc[dataset.ToStatus == start,
                            fields].sort_values(fields)
    from_status = dataset.loc[dataset.FromStatus == end,
                              fields].sort_values(fields)
    return to_status, from_status
