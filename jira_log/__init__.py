"""
Jira Log Module

Constants and common functions.
"""
JQL = ('project in (CVP, "Internal Team", "Partner Team", "uno API") '
       'and resolutiondate < 2019-12-01 and resolutiondate >= 2019-11-01')
JQL_MONTHLY = ('project in (CVP, Engineering, "Internal Team",'
               '"uno API") AND '
               '((status = Done AND resolutiondate >= {} ) OR '
               '(status not in (Backlog, "To Do", Done, Cancelled, Closed, '
               '"Selected for Development", Aborted, '
               'Discarded))) '
               'AND issuetype not in (Epic, Sub-task) '
               'AND CreatedDate < endOfMonth()'
               )
CONFIG_FOLDER = 'CONFIG_FOLDER'
OUTPUT_FOLDER = 'OUTPUT_FOLDER'
