#!/usr/bin/env python
# coding: utf-8
"""
Monthly Allocation Of Workers - CSV Creation
"""
import os
import pickle
from jira_log import OUTPUT_FOLDER
from jira_log.csv_dataset import create_dates_dataset


OUTPUT_FIELDS = ['TicketId', 'StartDate', 'EndDate', 'TimeInProgress',
                 'Type', 'EpicId', 'Asset', 'Phase', 'Employee',
                 ]
WIP_STATUS = 'IN PROGRESS'
QA_STATUS = 'IN QA'
UIUX_STATUS = ('ANALYSIS', 'VALIDATION')
WIP_CSV = 'jira_wip{}.csv'
QA_CSV = 'jira_wiqa{}.csv'
UIUX_CSV = 'jira_uiux{}.csv'


def create_file(dataset, status, file_name):
    dates = create_dates_dataset(dataset, status)
    rename_columns(dates)
    save_verification_file(dates, file_name)
    save_csv_file(dates, file_name)


def rename_columns(dataset):
    dataset.rename({'DateCreated_x': 'StartDate',
                    'DateCreated_y': 'EndDate',
                    'Author': 'Employee',
                    'DateCreated': 'DateMoved',
                    },
                   axis=1,
                   inplace=True)


def save_verification_file(dates, file_name):
    output_folder = os.environ.get(OUTPUT_FOLDER, '')
    pickle.dump(dates, open(os.path.join(output_folder,
                                         f'verify_{file_name}.pkl'), 'wb'))


def save_csv_file(dates, file_name):
    output_folder = os.environ.get(OUTPUT_FOLDER, '')
    dates[OUTPUT_FIELDS].to_csv(os.path.join(output_folder, file_name),
                                index=False)
