FROM python:3.7.6

# Container work dir
WORKDIR /app/jira_metrics

# Data and configuration files
COPY holidays.pkl ./
COPY requirements.txt ./

# Folder structure
RUN ["mkdir", "./jira_log"]
RUN ["mkdir", "./lib/"]
RUN ["mkdir", "./output/"]
RUN ["mkdir", "./config/"]

# Project files
ADD jira_log/ ./jira_log/
COPY lib/wallcards-0.1.1.tar.gz ./lib/

# Installing Python modules
RUN pip install -r requirements.txt
RUN pip install lib/wallcards-0.1.1.tar.gz

ENTRYPOINT ["python", "-m", "jira_log"]
